require_relative "../src/string_calculator"

describe StringCalculator do
  it 'given a empty string the calculator should be return 0' do
    
    expect(StringCalculator.add('')).to eq(0)
  end

  it 'given a zero as a string the calculator should be return 0' do
    
    expect(StringCalculator.add('0')).to eq(0)
  end

  it 'given a number 3 as a string the calculator should be return 3' do
    
    expect(StringCalculator.add('3')).to eq(3)
  end

  it 'given two numbers as a string the calculator should be return the sum of numbers' do
    
    expect(StringCalculator.add('3,2')).to eq(5)
  end

  it 'given an set of numbers as a string the calculator should be return the sum of numbers' do
    
    expect(StringCalculator.add('3,2,1,4')).to eq(10)
  end

  it 'given an set of numbers with an especial delimiter then the calculator should be return the sum of numbers' do
    
    expect(StringCalculator.add('1\n2,3')).to eq(6)
  end

  it 'given some negative numbers then the calculator should return an error message' do
    
    expect(StringCalculator.add('1,-2,-3')).to eq("deben de ser números positivos")
  end

  it 'given a number 3 digits long then claculator should return numbers without long number' do
    
    expect(StringCalculator.add('1,2,3000')).to eq(3)
  end
end
