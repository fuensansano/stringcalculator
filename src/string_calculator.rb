class StringCalculator 
  def self.add(number)
    return 0 if (number === '')
    return "deben de ser números positivos" if(number.include?('-'))

    number.strip!
    number.gsub!('\n',',')
    numbers = number.split(',').map { |number| number.to_i }
    numbers = numbers.select { |number| number < 1000 }

    numbers.sum
  
  end
end